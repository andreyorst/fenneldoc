LUA ?= lua
PREFIX ?= /usr/local
BINDIR = $(PREFIX)/bin
FENNEL ?= deps
FNLSOURCES = $(wildcard src/*.fnl)
FNLARGS = --no-metadata --require-as-include --compile
VERSION ?= $(shell git describe --abbrev=0 || "unknown")

.PHONY: build clean help install doc

build: $(FNLSOURCES)
	echo '#!/usr/bin/env $(LUA)' > fenneldoc
	echo 'FENNELDOC_VERSION = [[$(VERSION)]]' >> fenneldoc
	$(FENNEL) $(FNLARGS) src/fenneldoc.fnl >> fenneldoc
	chmod 755 fenneldoc
	./fenneldoc --config --project-version $(VERSION)

install: build
	mkdir -p $(BINDIR) && cp fenneldoc $(BINDIR)/

clean:
	rm -f fenneldoc $(wildcard src/*.lua)

doc:
	eval $$(deps --path); ./fenneldoc --no-sandbox $(FNLSOURCES)

lint:
	deps --fennel-ls
	fennel-ls --lint $(FNLSOURCES)

help:
	@echo "make         -- create executable lua script" >&2
	@echo "make clean   -- remove lua files" >&2
	@echo "make doc     -- generate documentation files for fenneldoc" >&2
	@echo "make install -- install fenneldoc accordingly to \$$PREFIX" >&2
	@echo "make help    -- print this message and exit" >&2
